/* For the main linear features, such as roads and railways. */
@track-fill: #C94500;
@path-fill: #FF0000;

@track-width-z12:                 1.5;
@track-width-z13:                 1.65;
@track-width-z14:                 1.8;
@track-width-z15:                 6;
@track-width-z16:                 7.3;
@track-width-z17:                 2.55;
@track-width-z18:                 2.8;
@track-width-z19:                 3.05;

@path-width-z12:               0.4;
@path-width-z13:               0.6;
@path-width-z14:               1.0;
@path-width-z15:               5.4;
@path-width-z16:               5.8;
@path-width-z17:               2.0;
@path-width-z18:               2.0;
@path-width-z19:               2.5;

@path-text-repeat-distance: 300;



#roads-fill[zoom >= 13][zoom <= 16]{
  ::fill {
    [feature = 'highway_bridleway'],
    [feature = 'highway_footway'],
    [feature = 'highway_cycleway'],
    [feature = 'highway_pedestrian'],
    [feature = 'highway_path']{
      [zoom >= 13]{
        line/line-width: @path-width-z12;
        line/line-color: @path-fill;
        //line/line-cap: round;
        //line/line-join: round;
        line/line-dasharray: 4,2;
        line/line-offset: 2;
        [zoom >= 13] { line/line-width: @path-width-z13; }
        [zoom >= 14] { line/line-width: @path-width-z14; }
        [zoom >= 15] { line/line-width: @path-width-z15;line/line-offset: 0;line/line-opacity:0.4; }
        [zoom >= 16] { line/line-width: @path-width-z16; }
        [zoom >= 17] { line/line-width: @path-width-z17; }
        [zoom >= 18] { line/line-width: @path-width-z18; }
        [zoom >= 19] { line/line-width: @path-width-z19; }
        [zoom >= 15]{
          line/line-dasharray: 14,5,6,5;    // TODO: ganz Kurze Linien nicht anzeigen, bzw. diese duenn
          [trail_visibility = '1'] {            line/line-dasharray: 18,4;          }
          [trail_visibility = '2'] {            line/line-dasharray: 16,6;          }
          [trail_visibility = '3'] {            line/line-dasharray: 14,8;          }
          [trail_visibility = '4'] {            line/line-dasharray: 12,10;          }
          [trail_visibility = '5'] {            line/line-dasharray: 7,13;          }
          [trail_visibility = '6'] {            line/line-dasharray: 5,15;          }
        }
      }
    }

    [feature = 'highway_track'] {
      [zoom >= 13] {
        line/line-color: @track-fill;
        line/line-dasharray: 5,4,2,4;
        //line/line-cap: round;
        line/line-join: round;
        line/line-clip:false;
        line/line-offset: 2;
        line/line-width: @track-width-z12;
        [zoom >= 13] { line/line-width: @track-width-z13; }
        [zoom >= 14] { line/line-width: @track-width-z14; }
        [zoom >= 15] { line/line-width: @track-width-z15;line/line-offset: 0;line/line-opacity:0.4; }
        [zoom >= 16] { line/line-width: @track-width-z16; }
        [zoom >= 17] { line/line-width: @track-width-z17; }
        [zoom >= 18] { line/line-width: @track-width-z18; }
        [zoom >= 19] { line/line-width: @track-width-z19; }
        [zoom >= 15]{
          line/line-dasharray: 14,5,6,5;
          [tracktype = 'grade1'] {            line/line-dasharray: 18,4;          }
          [tracktype = 'grade2'] {            line/line-dasharray: 16,6;          }
          [tracktype = 'grade3'] {            line/line-dasharray: 14,8;          }
          [tracktype = 'grade4'] {            line/line-dasharray: 12,10;          }
          [tracktype = 'grade5'] {            line/line-dasharray: 7,13;          }
        }
      }
    }
  }
}

#paths-text-name[zoom <= 16] {
  [highway = 'track'],
  [highway = 'construction'][construction = 'track'][zoom >= 14] {
    [zoom >= 15][zoom <= 17] {
      text-name: "[name]";
   //   [tracktype != null]{                  text-name: 'W'+[tracktype];      }
   //   [name != null][tracktype != null]{    text-name: [name]+' W'+[tracktype];      }
      text-fill: @track-fill;
      text-size: 9;  // Orginal: 8
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      text-spacing: @path-text-repeat-distance;
      text-clip: false;
      text-placement: line;
      text-face-name: @bold-fonts;
      text-vertical-alignment: middle;
      text-dy: 8;  // Orginal: 5
    }
    [zoom >= 16][zoom <= 17] {
      text-size: 10;  // Orginal: 9
      text-dy: 9;  // Orginal: 7
    }
    [zoom >= 17][zoom <= 17] {
      text-size: 11;  // Orginal: 11
      text-dy: 9;  // Orginal: 9
    }
  }

  [highway = 'bridleway'],
  [highway = 'footway'],
  [highway = 'cycleway'],
  [highway = 'path'],
  [highway = 'steps'],
  [highway = 'construction'][construction = 'bridleway'],
  [highway = 'construction'][construction = 'footway'],
  [highway = 'construction'][construction = 'cycleway'],
  [highway = 'construction'][construction = 'path'],
  [highway = 'construction'][construction = 'steps'] {
    [zoom >= 14][zoom <= 17] {
      text-name: "";
      [zoom >= 15] { text-name: "[name]"; } // show name of path only on zoom > 15
      [sac_scale != null] {
        text-name: 'T'+[sac_scale];
      }
      [mtb_scale != null] {
        text-name: 'S'+[mtb_scale];
      }
      [mtb_scale_uphill != null] {
        text-name: 'S'+[mtb_scale_uphill]+'up';
      }
      [name != null][sac_scale != null][zoom >= 15] {
        text-name: [name]+'  T'+[sac_scale];
      }
      [name != null][mtb_scale != null][zoom >= 15] {
        text-name: [name]+'  S'+[mtb_scale];
      }
      [name != null][mtb_scale_uphill != null][zoom >= 15] {
        text-name: [name]+'  S'+[mtb_scale_uphill]+'up';
      }
      [mtb_scale != null][sac_scale != null] {
        text-name: 'T'+[sac_scale]+'  S'+[mtb_scale];
      }
      [sac_scale != null][mtb_scale_uphill != null] {
        text-name: 'T'+[sac_scale]+'  S'+[mtb_scale_uphill]+'up';
      }
      [mtb_scale != null][mtb_scale_uphill != null] {
        text-name: 'S'+[mtb_scale]+'  S'+[mtb_scale_uphill]+'up';
      }
      [name != null][mtb_scale != null][mtb_scale_uphill != null][zoom >= 15] {
        text-name: [name]+'  S'+[mtb_scale]+'  S'+[mtb_scale_uphill]+'up';
      }
      [name != null][sac_scale != null][mtb_scale != null][zoom >= 15] {
        text-name: [name]+'  T'+[sac_scale]+'  S'+[mtb_scale];
      }
      [name != null][sac_scale != null][mtb_scale_uphill != null][zoom >= 15] {
        text-name: [name]+'  T'+[sac_scale]+'  S'+[mtb_scale_uphill]+'up';
      }
      [sac_scale != null][mtb_scale != null][mtb_scale_uphill != null] {
        text-name: 'T'+[sac_scale]+'  S'+[mtb_scale]+'  S'+[mtb_scale_uphill]+'up';
      }
      [name != null][sac_scale != null][mtb_scale != null][mtb_scale_uphill != null][zoom >= 15] {
        text-name: [name]+'  T'+[sac_scale]+'  S'+[mtb_scale]+'  S'+[mtb_scale_uphill]+'up';
      }
      text-fill: @path-fill;
      text-size: 9;  // Orginal: 9
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      text-spacing: @path-text-repeat-distance;
      text-clip: false;
      text-placement: line;
      text-face-name: @bold-fonts;
      text-vertical-alignment: middle;
      text-dy: 8;  // Orginal: 7
    }
    [zoom >= 16][zoom <= 17] {
      text-size: 10;  // Orginal: 9
      text-dy: 9;  // Orginal: 7
    }
    [zoom >= 17][zoom <= 17] {
      text-size: 11;
      text-dy: 9;
    }
  }
}

