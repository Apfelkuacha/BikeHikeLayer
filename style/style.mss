Map {
  background-color: transparent;
}

@water-color: #aad3df;
@land-color: #f2efe9;

@standard-halo-radius: 2;
//@standard-halo-fill: rgba(255,255,255,0.6);
@standard-halo-fill: fadeout(white, 30%);
