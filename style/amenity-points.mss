@water-color: #00B5FF;
@water-text: #00B5FF;
@glacier: #ddecec;
@glacier-line: #9cf;
@shop-icon: #ac39ac;
@shop-text: #939;
@transportation-icon: #0092da;
@transportation-text: #0066ff;
@accommodation-icon: @transportation-icon;
@accommodation-text: @transportation-text;

@amenity-brown: #734a08;
@gastronomy-icon: #C77400;
@gastronomy-text: darken(@gastronomy-icon, 5%);
@memorials: @amenity-brown;
@culture: @amenity-brown;
@public-service: @amenity-brown;
@man-made-icon: #666666;
@barrier-icon: #3f3f3f;
@landform-color: #d08f55;
@leisure-green: darken(@park, 60%);
@religious-icon: #000000;

@park: #c8facc;         // Lch(94,30,145)

// --- Other ----

@bare_ground: #eee5dc;
@construction: #c7c7b4; // also brownfield
@leisure: lighten(@park, 5%);
@sand: #f5e9c6;
@tourism: #660033;
@peak-color: black;
@peak-font-color: black;

@landcover-font-size: 10;
@landcover-wrap-width-size: 30; // 3 em
@landcover-line-spacing-size: -1.5; // -0.15 em
@landcover-font-size-big: 12;
@landcover-wrap-width-size-big: 36; // 3 em
@landcover-line-spacing-size-big: -1.8; // -0.15 em
@landcover-font-size-bigger: 15;
@landcover-wrap-width-size-bigger: 45; // 3 em
@landcover-line-spacing-size-bigger: -2.25; // -0.15 em
@landcover-face-name: @oblique-fonts;

@standard-font-size: 10;
@standard-wrap-width: 30; // 3 em
@standard-line-spacing-size: -1.5; // -0.15 em
@standard-font: @book-fonts;

#mountains[zoom >= 7][zoom <= 14] {
  [feature = 'natural_peak'][zoom = 7][otm_isolation > 40000],
  [feature = 'natural_peak'][zoom = 8][otm_isolation > 20000],
  [feature = 'natural_peak'][zoom = 9][otm_isolation > 9000],
  [feature = 'natural_peak'][zoom = 10][otm_isolation > 4000],
  [feature = 'natural_peak'][zoom = 11][otm_isolation > 1800],
  [feature = 'natural_peak'][zoom = 12][otm_isolation > 400]
  {
    marker-width: 5;
    [zoom < 11] { marker-width: 4 }
    [zoom < 10] { marker-width: 4 }
    [zoom < 9] { marker-width: 4 }
    marker-file: url('symbols/natural/peak.svg');
    marker-fill: @peak-color;
    marker-clip: false;
  }

  [feature = 'natural_volcano'][zoom = 7][otm_isolation > 40000],
  [feature = 'natural_volcano'][zoom = 8][otm_isolation > 20000],
  [feature = 'natural_volcano'][zoom = 9][otm_isolation > 9000],
  [feature = 'natural_volcano'][zoom = 10][otm_isolation > 4000],
  [feature = 'natural_volcano'][zoom = 11][otm_isolation > 1800],
  [feature = 'natural_volcano'][zoom = 12][otm_isolation > 400] {
    marker-width: 5;
    [zoom < 11] { marker-width: 4 }
    [zoom < 10] { marker-width: 3 }
    marker-file: url('symbols/natural/peak.svg');
    marker-fill: #d40000;
    marker-clip: false;
  }

  [feature = 'natural_saddle'][zoom >= 14][zoom < 15] {
    marker-file: url('symbols/natural/saddle.svg');
    marker-fill: @landform-color;
    marker-clip: false;
  }

  [feature = 'mountain_pass'][zoom >= 13][zoom < 15] {
    marker-file: url('symbols/natural/saddle.svg');
    marker-fill: @peak-color;
    marker-clip: false;
  }
    
  [feature = 'natural_peak'][zoom = 7][otm_isolation > 40000],
  [feature = 'natural_peak'][zoom = 8][otm_isolation > 20000],
  [feature = 'natural_peak'][zoom = 9][otm_isolation > 9000],
  [feature = 'natural_peak'][zoom = 10][otm_isolation > 4000],
  [feature = 'natural_peak'][zoom = 11][otm_isolation > 1800],
  [feature = 'natural_peak'][zoom = 12][otm_isolation > 400],
  [feature = 'natural_volcano'][zoom = 7][otm_isolation > 40000],
  [feature = 'natural_volcano'][zoom = 8][otm_isolation > 20000],
  [feature = 'natural_volcano'][zoom = 9][otm_isolation > 9000],
  [feature = 'natural_volcano'][zoom = 10][otm_isolation > 4000],
  [feature = 'natural_volcano'][zoom = 11][otm_isolation > 1800],
  [feature = 'natural_volcano'][zoom = 12][otm_isolation > 400],
  [feature = 'natural_saddle'][zoom >= 14][zoom < 15],
  [feature = 'mountain_pass'][zoom >= 14][zoom < 15],
  [feature = 'tourism_viewpoint'][zoom >= 14][zoom < 16] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @peak-font-color;
    [feature = 'mountain_pass'] { text-fill: @peak-font-color; }
    text-dy: 7;
    [feature = 'tourism_viewpoint'] { text-dy: 11; }
    text-face-name: @bold-fonts;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }
}


#amenity-points[zoom <= 16] {
  [feature = 'tourism_alpine_hut'][zoom >= 11][zoom <= 13],
  [feature = 'tourism_wilderness_hut'][zoom >= 11][zoom <= 13],
  [feature = 'amenity_shelter'][zoom >= 13][zoom <= 13] {
      
    [zoom < 13] { marker-width: 7 }
    [zoom < 11] { marker-width: 5 }
    marker-file: url('symbols/amenity/shelter.svg');
    [feature = 'tourism_wilderness_hut'] {
      marker-file: url('symbols/tourism/wilderness_hut.svg');
    }
    [feature = 'tourism_alpine_hut'] {
      marker-file: url('symbols/tourism/alpinehut.svg');
    }
    [feature = 'amenity_shelter'] {
      marker-fill: @man-made-icon;
    }
    marker-fill: @accommodation-icon;
    marker-clip: false;
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    [access != ''][access != 'permissive'][access != 'yes'] {      marker-opacity: 0.33;    }
  }
    
  [feature = 'highway_bus_stop'] {
    [zoom >= 15][zoom < 16] {
      marker-file: url('symbols/square.svg');
      marker-fill: @transportation-icon;
      marker-width: 3;
      marker-clip: false;
    }
    [zoom >= 16][zoom < 17] {
      marker-file: url('symbols/highway/bus_stop.svg');
      marker-fill: @transportation-icon;
      marker-clip: false;
    }
  }

  [feature = 'amenity_bus_station'][zoom >= 15] {
    marker-file: url('symbols/amenity/bus_station.svg');
    // use colors from SVG to allow for white background
    marker-clip: false;
  }

  [feature = 'amenity_fountain'] {
    ::basin {
      [zoom >= 13] {
        marker-fill: @water-color;
        marker-line-width: 0;
        marker-width: 4;
        marker-allow-overlap: true;
        marker-ignore-placement: true;
      }
    }
    ::nozzle {
      [zoom >= 14] {
        nozzle/marker-fill: @water-color;
        nozzle/marker-file: url('symbols/amenity/fountain.svg');
        nozzle/marker-clip: false;
        nozzle/marker-allow-overlap: true;
        nozzle/marker-ignore-placement: true;
      }
    }
  }

  [feature = 'amenity_shower'][zoom >= 16] {
    marker-file: url('symbols/amenity/shower.svg');
    marker-fill: @water-color;
    marker-clip: false;
    [access != ''][access != 'permissive'][access != 'yes'] {      marker-opacity: 0.33;    }
  }

  [feature = 'amenity_drinking_water'][zoom >= 13] {
    [zoom >= 13][zoom < 14] {
      marker-file: url('symbols/square.svg');
      marker-fill: @water-color;
      marker-width: 4;
      marker-clip: false;
    }
    [zoom >= 14] {
      marker-file: url('symbols/amenity/drinking_water.svg');
      marker-fill: @water-color;
      marker-clip: false;
      marker-allow-overlap: true;
      //marker-ignore-placement: true;
    }
    [access != ''][access != 'permissive'][access != 'yes'] {      marker-opacity: 0.33;    }
  }

  [feature = 'tourism_viewpoint'][zoom >= 14] {
    marker-file: url('symbols/tourism/viewpoint.svg');
    marker-fill: @amenity-brown;
    marker-clip: false;
  }
      
  /*[feature = 'natural_peak'][zoom >= 9][zoom < 10][otm_isolation > 12000],
  [feature = 'natural_peak'][zoom >= 10][zoom < 11][otm_isolation > 6000],
  [feature = 'natural_peak'][zoom >= 11][zoom < 12][otm_isolation > 1800],
  [feature = 'natural_peak'][zoom >= 12][zoom < 13][otm_isolation > 400],
  {
    marker-width: 5;
    [zoom < 11] { marker-width: 4 }
    [zoom < 10] { marker-width: 3 }
    marker-file: url('symbols/natural/peak.svg');
    marker-fill: @peak-color;
    marker-clip: false;
  }

  [feature = 'natural_volcano'][zoom >= 9][zoom < 10][otm_isolation > 12000],
  [feature = 'natural_volcano'][zoom >= 10][zoom < 11][otm_isolation > 6000],
  [feature = 'natural_volcano'][zoom >= 11][zoom < 12][otm_isolation > 1800],
  [feature = 'natural_volcano'][zoom >= 12][zoom < 13][otm_isolation > 400] {
    marker-width: 5;
    [zoom < 11] { marker-width: 4 }
    [zoom < 10] { marker-width: 3 }
    marker-file: url('symbols/natural/peak.svg');
    marker-fill: #d40000;
    marker-clip: false;
  }

  [feature = 'natural_saddle'][zoom >= 14][zoom < 15] {
    marker-file: url('symbols/natural/saddle.svg');
    marker-fill: @landform-color;
    marker-clip: false;
  }

  [feature = 'mountain_pass'][zoom >= 13][zoom < 15] {
    marker-file: url('symbols/natural/saddle.svg');
    marker-fill: @peak-color;
    marker-clip: false;
  }*/

  [feature = 'natural_spring'][zoom >= 13] {
    marker-file: url('symbols/natural/spring.svg');
    marker-fill: #7abcec;
    marker-clip: false;
  }

  [feature = 'natural_cave_entrance'][zoom >= 13] {
    marker-file: url('symbols/natural/cave.svg');
    marker-clip: false;
  }

  [feature = 'waterway_waterfall'] {
    [zoom >= 12][height > 20],
    [zoom >= 13][height > 10],
    [zoom >= 14] {
      marker-file: url('symbols/natural/waterfall.svg');
      marker-clip: false;
      marker-fill: @water-text;
    }
  }

  [feature = 'amenity_hunting_stand'][zoom >= 15] {
    marker-file: url('symbols/amenity/hunting_stand.svg');
    marker-fill: @man-made-icon;
    marker-clip: false;
  }

  [feature = 'tourism_picnic_site'][zoom >= 14] {
    marker-file: url('symbols/tourism/picnic.svg');
    marker-fill: @leisure-green;
    marker-clip: false;
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    [access != ''][access != 'permissive'][access != 'yes'] {      marker-opacity: 0.33;    }
  }

  [feature = 'leisure_picnic_table'][zoom >= 15] {
    marker-file: url('symbols/tourism/picnic.svg');
    marker-fill: @man-made-icon;
    marker-clip: false;
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    [access != ''][access != 'permissive'][access != 'yes'] {      marker-opacity: 0.33;    }
  }

  [feature = 'leisure_firepit'][zoom >= 16] {
    marker-file: url('symbols/leisure/firepit.svg');
    marker-fill: @amenity-brown;
    marker-clip: false;
    [access != ''][access != 'permissive'][access != 'yes'] {      marker-opacity: 0.33;    }
  }

  [feature = 'leisure_sauna'][zoom >= 16] {
     marker-file: url('symbols/leisure/sauna.svg');
     marker-fill: @leisure-green;
     marker-clip: false;
   }

  [feature = 'leisure_outdoor_seating'][zoom >= 16] {
     marker-file: url('symbols/leisure/outdoor_seating.svg');
     marker-fill: @leisure-green;
     marker-clip: false;
   }

  [feature = 'leisure_bird_hide'][zoom >= 16] {
     marker-file: url('symbols/leisure/bird_hide.svg');
     marker-fill: @leisure-green;
     marker-clip: false;
   }

  [feature = 'amenity_parking'],
  [feature = 'amenity_bicycle_parking'],
  [feature = 'amenity_motorcycle_parking'],
  [feature = 'amenity_parking_entrance'] {
    [zoom >= 14][way_pixels > 1500]["parking" != 'street_side']["parking" != 'lane'],
    [zoom >= 15][feature = 'amenity_parking']["parking" != 'street_side']["parking" != 'lane'],
    [zoom >= 16] {
      [feature = 'amenity_parking'] { marker-file: url('symbols/amenity/parking.svg'); }
      [feature = 'amenity_parking']["parking" = 'street_side'],
      [feature = 'amenity_parking']["parking" = 'lane'] { 
        marker-file: url('symbols/amenity/parking_subtle.svg'); 
      }
      [feature = 'amenity_bicycle_parking'] { marker-file: url('symbols/amenity/bicycle_parking.svg'); }
      [feature = 'amenity_motorcycle_parking'] { marker-file: url('symbols/amenity/motorcycle_parking.svg'); }
      [feature = 'amenity_parking_entrance']["parking"='underground'] { marker-file: url('symbols/amenity/parking_entrance_underground.svg'); }
      [feature = 'amenity_parking_entrance']["parking"='multi-storey'] { marker-file: url('symbols/amenity/parking_entrance_multistorey.svg'); }
      marker-clip: false;
      marker-fill: @transportation-icon;
      [access != ''][access != 'permissive'][access != 'yes'] { marker-opacity: 0.33; }
    }
  }

  [feature = 'historic_memorial'][memorial = null][zoom >= 15],
  [feature = 'historic_memorial'][memorial != null][memorial != 'blue_plaque'][memorial != 'bust'][memorial != 'plaque'][memorial != 'stele'][memorial != 'stone'][zoom >= 15],
  [feature = 'historic_memorial'][memorial = 'statue'][zoom >= 15],
  [feature = 'historic_memorial'][memorial = 'bust'][zoom >= 16],
  [feature = 'historic_memorial'][memorial = 'stele'][zoom >= 16],
  [feature = 'historic_memorial'][memorial = 'stone'][zoom >= 16],
  [feature = 'historic_memorial'][memorial = 'blue_plaque'][zoom >= 16],
  [feature = 'historic_memorial'][memorial = 'plaque'][zoom >= 16] {
    marker-file: url('symbols/historic/memorial.svg');
    [memorial = 'bust']{
      marker-file: url('symbols/historic/bust.svg');
    }
    [memorial = 'blue_plaque'],
    [memorial = 'plaque'] {
      marker-file: url('symbols/historic/plaque.svg');
    }
    [memorial = 'statue'] {
      marker-file: url('symbols/historic/statue.svg');
    }
    [memorial = 'stone'] {
      marker-file: url('symbols/historic/stone.svg');
    }
    marker-fill: @memorials;
    marker-clip: false;
  }

  [feature = 'man_made_obelisk'][zoom >= 15] {
    marker-file: url('symbols/historic/obelisk.svg');
    marker-fill: @memorials;
    marker-clip: false;
  }

  [feature = 'historic_monument'][zoom >= 14] {
    marker-file: url('symbols/historic/monument.svg');
    marker-fill: @memorials;
    marker-clip: false;
  }

  [feature = 'historic_fort'][zoom >= 14] {
    marker-file: url('symbols/historic/fort.svg');
    marker-fill: @memorials;
    marker-clip: false;
  }

  [feature = 'historic_castle'][castle_type != 'stately'][castle_type != 'manor'][zoom >= 13],
  [feature = 'historic_castle'][castle_type = 'stately'][zoom >= 14],
  [feature = 'historic_castle'][castle_type = 'manor'][zoom >= 14],
  [feature = 'historic_manor'][zoom >= 14] {
    marker-file: url('symbols/historic/castle.svg');
    marker-fill: @memorials;
    marker-clip: false;
    [castle_type = 'palace'],
    [castle_type = 'stately'] {
      marker-file: url('symbols/historic/palace.svg');
    }
    [castle_type = 'manor'],
    [feature = 'historic_manor'] {
      marker-file: url('symbols/historic/manor.svg');
    }
    [castle_type = 'fortress'],
    [castle_type = 'defensive'],
    [castle_type = 'castrum'],
    [castle_type = 'shiro'],
    [castle_type = 'kremlin'] {
      marker-file: url('symbols/historic/fortress.svg');
    }
  }

  [feature = 'historic_archaeological_site'][zoom >= 14] {
    marker-file: url('symbols/historic/archaeological_site.svg');
    marker-fill: @culture;
    marker-clip: false;
  }

  [feature = 'amenity_marketplace'][zoom >= 14][way_pixels > 3000],
  [feature = 'amenity_marketplace'][zoom >= 15] {
    marker-clip: false;
    marker-fill: @shop-icon;
    marker-file: url('symbols/shop/marketplace.svg');
  }


  [feature = 'amenity_atm'][zoom >= 16] {
    marker-file: url('symbols/amenity/atm.svg');
    marker-fill: @amenity-brown;
    marker-clip: false;
  }

  [feature = 'amenity_bureau_de_change'][zoom >= 16] {
    marker-file: url('symbols/amenity/bureau_de_change.svg');
    marker-fill: @amenity-brown;
    marker-clip: false;
  }

  [feature = 'amenity_bank'][zoom >= 16] {
    marker-file: url('symbols/amenity/bank.svg');
    marker-fill: @public-service;
    marker-clip: false;
  }

  [feature = 'tourism_gallery'][zoom >= 16] {
    marker-file: url('symbols/shop/art.svg');
    marker-clip: false;
    marker-fill: @amenity-brown;
  }

  [feature = 'tourism_hostel'][zoom >= 15] {
    marker-file: url('symbols/tourism/hostel.svg');
    marker-clip: false;
    marker-fill: @accommodation-icon;
  }

  [feature = 'tourism_hotel'][zoom >= 15] {
    marker-file: url('symbols/tourism/hotel.svg');
    marker-clip: false;
    marker-fill: @accommodation-icon;
  }

  [feature = 'tourism_motel'][zoom >= 15] {
    marker-file: url('symbols/tourism/motel.svg');
    marker-clip: false;
    marker-fill: @accommodation-icon;
  }

  [feature = 'tourism_information'][zoom >= 16],
  [feature = 'tourism_information']["information"='office'][zoom >= 14] {
    [information = 'audioguide'] {
      marker-file: url('symbols/tourism/audioguide.svg');
    }
    [information = 'board'] {
      marker-file: url('symbols/tourism/board.svg');
    }
    [information = 'guidepost'] {
      marker-file: url('symbols/tourism/guidepost.svg');
    }
    [information = 'office'] {
      marker-file: url('symbols/tourism/office.svg');
      marker-fill: @amenity-brown;
    }
    [information = 'map'],
    [information = 'tactile_map'] {
      marker-file: url('symbols/tourism/map.svg');
    }
    [information = 'terminal'] {
      marker-file: url('symbols/tourism/terminal.svg');
    }
      marker-fill: @man-made-icon;
      marker-clip: false;
  }

  [feature = 'shop'] {
    [shop != 'mall'][shop != 'massage'][zoom >= 15],
    [shop = 'supermarket'][zoom >= 14],
    [shop = 'department_store'][zoom >= 14] {
      marker-clip: false;
      marker-fill: @shop-icon;
      marker-width: 5;
    }

    [zoom >= 15][zoom < 18][shop != 'supermarket'][shop != 'department_store'][shop != 'mall'][shop != 'massage'] {
      marker-width: 4;
      marker-line-width: 0;
    }
    [shop = 'other'][zoom >= 15] {
      marker-width: 6;
      marker-line-width: 0;
    }
    [shop = 'supermarket'][zoom >= 15] {      marker-file: url('symbols/shop/supermarket.svg');    }
    [shop = 'bag'][zoom >= 15] {      marker-file: url('symbols/shop/bag.svg');    }
    [shop = 'bakery'][zoom >= 15] {      marker-file: url('symbols/shop/bakery.svg');    }
    [shop = 'beverages'][zoom >= 15] {      marker-file: url('symbols/shop/beverages.svg');    }
    [shop = 'bookmaker'][zoom >= 16] {      marker-file: url('symbols/shop/bookmaker.svg');    }
    [shop = 'books'][zoom >= 16] {      marker-file: url('symbols/amenity/library.svg');    }
    [shop = 'butcher'][zoom >= 15] {      marker-file: url('symbols/shop/butcher.svg');    }
    [shop = 'carpet'][zoom >= 16] {      marker-file: url('symbols/shop/carpet.svg');    }
    [shop = 'charity'][zoom >= 16] {      marker-file: url('symbols/shop/charity.svg');    }
    [shop = 'chemist'][zoom >= 16] {      marker-file: url('symbols/shop/chemist.svg');    }
    [shop = 'coffee'][zoom >= 15] {      marker-file: url('symbols/shop/coffee.svg');    }
    [shop = 'convenience'][zoom >= 15] {      marker-file: url('symbols/shop/convenience.svg');    }
    [shop = 'chocolate'][zoom >= 15],
    [shop = 'confectionery'][zoom >= 15],
    [shop = 'pastry'][zoom >= 15] {      marker-file: url('symbols/shop/confectionery.svg');    }
    [shop = 'deli'][zoom >= 15] {        marker-file: url('symbols/shop/deli.svg');      }
    [shop = 'department_store'][zoom >= 15] {      marker-file: url('symbols/shop/department_store.svg');    }
    [shop = 'fishmonger'],
    [shop = 'seafood'] {      [zoom >= 16] {        marker-file: url('symbols/shop/seafood.svg');      }    }
    [shop = 'greengrocer'],
    [shop = 'farm'] {      [zoom >= 16] {        marker-file: url('symbols/shop/greengrocer.svg');      }    }
    [shop = 'bicycle'][zoom >= 15] {      marker-file: url('symbols/shop/bicycle.svg');    }
    [shop = 'alcohol'],
    [shop = 'wine'] {      [zoom >= 16] {        marker-file: url('symbols/shop/alcohol.svg');      }    }
    [shop = 'outdoor'][zoom >= 16] {      marker-file: url('symbols/shop/outdoor.svg');    }
    [shop = 'medical_supply'][zoom >= 16]{      marker-file: url('symbols/shop/medical_supply.svg');    }
    [shop = 'kiosk'],
    [shop = 'newsagent'] {      [zoom >= 16] {        marker-file: url('symbols/shop/newsagent.svg');      }    }
    [shop = 'second_hand'][zoom >= 16] {      marker-file: url('symbols/shop/second_hand.svg');    }
    [shop = 'sports'][zoom >= 16] {      marker-file: url('symbols/shop/sports.svg');    }
    [shop = 'stationery'][zoom >= 16] {      marker-file: url('symbols/shop/stationery.svg');    }
    [shop = 'tea'][zoom >= 16] {      marker-file: url('symbols/shop/tea.svg');    }
    [shop = 'variety_store'][zoom >= 16] {      marker-file: url('symbols/shop/variety_store.svg');    }
  }

  [feature = 'leisure_water_park'][zoom >= 16],
  [feature = 'leisure_sports_centre'][sport = 'swimming'][zoom >= 16],
  [feature = 'leisure_swimming_area'][zoom >= 16] {
    marker-file: url('symbols/leisure/water_park.svg');
    marker-fill: @leisure-green;
    marker-clip: false;
  }

  [feature = 'leisure_beach_resort'][zoom >= 15] {
     marker-file: url('symbols/leisure/beach_resort.svg');
     marker-fill: @leisure-green;
     marker-clip: false;
   }

  [feature = 'leisure_amusement_arcade'][zoom >= 16] {
     marker-file: url('symbols/leisure/amusement_arcade.svg');
     marker-fill: @leisure-green;
     marker-clip: false;
   }

  [feature = 'leisure_fishing'][zoom >= 16] {
     marker-file: url('symbols/leisure/fishing.svg');
     marker-fill: @leisure-green;
     marker-clip: false;
  }
}

#amenity-low-priority[zoom <= 16] {
  [feature = 'man_made_cross'][zoom >= 14],
  [feature = 'historic_wayside_cross'][zoom >= 14] {
    marker-file: url('symbols/man_made/cross.svg');
    marker-fill: @religious-icon;
    marker-clip: false;
  }

  [feature = 'historic_wayside_shrine'][zoom >= 15] {
    marker-file: url('symbols/historic/shrine.svg');
    marker-fill: @man-made-icon;
    marker-clip: false;
  }

  [feature = 'barrier_gate']::barrier {
    [zoom >= 15] {
      marker-file: url('symbols/barrier/gate.svg');
      marker-clip: false;
    }
  }

  [feature = 'barrier_lift_gate'][zoom >= 15]::barrier,
  [feature = 'barrier_swing_gate'][zoom >= 15]::barrier {
    marker-file: url('symbols/barrier/lift_gate.svg');
    marker-fill: @barrier-icon;
    marker-clip: false;
  }

  [feature = 'barrier_cattle_grid'][zoom >= 15]::barrier {
    marker-file: url('symbols/barrier/cattle_grid.svg');
    marker-fill: @barrier-icon;
    marker-clip: false;
  }

  [feature = 'barrier_stile'][zoom >= 15]::barrier {
    marker-file: url('symbols/barrier/stile.svg');
    marker-fill: @barrier-icon;
    marker-clip: false;
  }

  [feature = 'barrier_motorcycle_barrier'][zoom >= 15]::barrier {
    marker-file: url('symbols/barrier/motorcycle_barrier.svg');
    marker-fill: @barrier-icon;
    marker-clip: false;
  }

  [feature = 'barrier_cycle_barrier'][zoom >= 15]::barrier {
    marker-file: url('symbols/barrier/cycle_barrier.svg');
    marker-fill: @barrier-icon;
    marker-clip: false;
  }

  [feature = 'barrier_full-height_turnstile'][zoom >= 15]::barrier {
    marker-file: url('symbols/barrier/full-height_turnstile.svg');
    marker-fill: @barrier-icon;
    marker-clip: false;
  }

  [feature = 'barrier_kissing_gate'][zoom >= 15]::barrier {
    marker-file: url('symbols/barrier/kissing_gate.svg');
    marker-fill: @barrier-icon;
    marker-clip: false;
  }

  [feature = 'barrier_bollard'],
  [feature = 'barrier_block'],
  [feature = 'barrier_log'],
  [feature = 'barrier_turnstile'] {
    [zoom >= 15] {
      marker-width: 3;
      marker-line-width: 0;
      marker-fill: #7d7c7c;

      [zoom >= 16] {
        marker-width: 4;
      }
    }
  }

  [feature = 'amenity_bench'][zoom >= 16]::amenity {
    marker-file: url('symbols/amenity/bench.svg');
    marker-fill: @man-made-icon;
    [access != ''][access != 'permissive'][access != 'yes'] {      marker-opacity: 0.33;    }
  }

  [feature = 'amenity_waste_basket'][zoom >= 17]::amenity {
    marker-file: url('symbols/amenity/waste_basket.svg');
    marker-fill: @man-made-icon;
    [access != ''][access != 'permissive'][access != 'yes'] {      marker-opacity: 0.33;    }
  }

  // waste_disposal tagging on nodes - tagging on ways is defined earlier
  [feature = 'amenity_waste_disposal'][zoom >= 17]::amenity {
    [access = null],
    [access = 'permissive'],
    [access = 'yes'] {
      marker-file: url('symbols/amenity/waste_disposal.svg');
      marker-fill: @man-made-icon;
    }
  }
}

#text-point[zoom >= 9][zoom <= 16] {

  [feature = 'place_locality'][zoom >= 13][zoom <= 15] {
    text-name: "[name]";
    text-size: 10;
    text-fill: @placenames;
    text-face-name: @bold-fonts;
    text-halo-fill: @standard-halo-fill;
    text-halo-radius: @standard-halo-radius * 1.5;
    text-wrap-width: 45; // 4.5 em
    text-line-spacing: -0.8; // -0.08 em
    text-margin: 7.0; // 0.7 em
    text-placement-type: simple;
    text-placements: "E,NE,SE,W,N,NW,SW,10,9,8,7,6";
    text-dx: 5;
    text-dy: 5;
    [zoom >= 17] {
      text-size: 12;
      text-wrap-width: 60; // 5.0 em
      text-line-spacing: -0.60; // -0.05 em
      text-margin: 8.4; // 0.7 em
      text-fill: @placenames-light;
      text-halo-fill: white;
    }
  }

  [feature = 'place_square'][zoom >= 13][zoom <= 15] {
    text-name: "[name]";
    text-size: 11;
    text-face-name: @bold-fonts;
    text-halo-fill: @standard-halo-fill;
    text-halo-radius: @standard-halo-radius * 1.5;
    text-wrap-width: 45; // 4.5 em
    text-line-spacing: -0.8; // -0.08 em
    text-margin: 7.0; // 0.7 em
  }
/*
  [feature = 'natural_peak'][zoom >= 9][zoom < 10][otm_isolation > 15000],
  [feature = 'natural_peak'][zoom >= 10][zoom < 11][otm_isolation > 8000],
  [feature = 'natural_peak'][zoom >= 11][zoom < 12][otm_isolation > 2000],
  [feature = 'natural_peak'][zoom >= 12][zoom < 13],
  [feature = 'natural_volcano'][zoom >= 9][zoom < 10][otm_isolation > 15000],
  [feature = 'natural_volcano'][zoom >= 10][zoom < 11][otm_isolation > 8000],
  [feature = 'natural_volcano'][zoom >= 11][zoom < 12][otm_isolation > 2000],
  [feature = 'natural_volcano'][zoom >= 12][zoom < 13],
  [feature = 'natural_saddle'][zoom >= 13][zoom < 15],
  [feature = 'mountain_pass'][zoom >= 13][zoom < 15],
  [feature = 'tourism_viewpoint'][zoom >= 14][zoom < 16] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @peak-font-color;
    [feature = 'mountain_pass'] { text-fill: @peak-font-color; }
    text-dy: 7;
    [feature = 'tourism_viewpoint'] { text-dy: 11; }
    text-face-name: @bold-fonts;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }
*/
  [feature = 'amenity_pub'][zoom >= 16],
  [feature = 'amenity_restaurant'][zoom >= 16],
  [feature = 'amenity_food_court'][zoom >= 16],
  [feature = 'amenity_cafe'][zoom >= 16],
  [feature = 'amenity_fast_food'][zoom >= 16],
  [feature = 'amenity_biergarten'][zoom >= 16],
  [feature = 'amenity_bar'][zoom >= 16],
  [feature = 'amenity_ice_cream'][zoom >= 16] {
    text-name: "[name]";
    text-fill: @gastronomy-text;
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-dy: 11;
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
    [feature = 'amenity_bar'] {
      text-dy: 13;
    }
  }

  [feature = 'amenity_library'],
  [feature = 'tourism_museum'],
  [feature = 'amenity_theatre'],
  [feature = 'amenity_cinema'],
  [feature = 'amenity_arts_centre'],
  [feature = 'amenity_community_centre'],
  [feature = 'historic_archaeological_site'],
  [feature = 'amenity_nightclub'] {
    [zoom >= 16] {
      text-name: "[name]";
      text-size: @standard-font-size;
      text-wrap-width: @standard-wrap-width;
      text-line-spacing: @standard-line-spacing-size;
      text-fill: @culture;
      text-dy: 11;
      [feature = 'amenity_community_centre'] { text-dy: 10; }
      text-face-name: @standard-font;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      [feature = 'amenity_nightclub']{
        text-dy: 12;
      }
    }
  }

  [feature = 'amenity_public_bath'][zoom >= 16] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @amenity-brown;
    text-dy: 11;
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }

  [feature = 'leisure_sauna'][zoom >= 16] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @leisure-green;
    text-dy: 11;
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }

  [feature = 'amenity_bicycle_rental'][zoom >= 16],
  [feature = 'barrier_toll_booth'][zoom >= 16],
  [feature = 'leisure_slipway'][zoom >= 16] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @transportation-text;
    [feature = 'amenity_car_rental']     { text-dy: 10; }
    [feature = 'amenity_bicycle_rental'] { text-dy: 10; }
    [feature = 'amenity_boat_rental']    { text-dy: 13; }
    [feature = 'barrier_toll_booth']     { text-dy: 13; }
    [feature = 'leisure_slipway']        { text-dy: 13; }
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }


  [feature = 'amenity_bbq'][zoom >= 16],
  [feature = 'amenity_bicycle_repair_station'][zoom >= 16],
  [feature = 'amenity_drinking_water'][zoom >= 15],
  [feature = 'amenity_shower'][zoom >= 16],
  [feature = 'tourism_picnic_site'][zoom >= 16] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @amenity-brown;
    [feature = 'tourism_picnic_site'] {
      text-fill: @leisure-green;
    }
    [feature = 'amenity_drinking_water'] {
      text-fill: @water-color;
    }
    text-dy: 10;
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
    [access != ''][access != 'permissive'][access != 'yes'] {
      text-opacity: 0.33;
      text-halo-radius: 0;
    }
  }

  [feature = 'tourism_information'][zoom >= 16],
  [feature = 'tourism_information']["information"='office'][zoom >= 15] {
      text-name: "[name]";
      text-size: @standard-font-size;
      text-wrap-width: @standard-wrap-width;
      text-line-spacing: @standard-line-spacing-size;
      text-fill: darken(black, 30%);
      [information = 'office'] { text-fill: @amenity-brown; }
      text-face-name: @standard-font;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      text-dy: 11;
  }

  [feature = 'waterway_waterfall'] {
    [zoom >= 12][height > 20],
    [zoom >= 13][height > 10],
    [zoom >= 14][name != null],
    [zoom >= 15] {
      text-name: "[name]";
      text-size: @standard-font-size;
      text-wrap-width: @standard-wrap-width;
      text-line-spacing: @standard-line-spacing-size;
      text-fill: @water-text;
      text-dy: 10;
      text-face-name: @standard-font;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
    }
  }

  [feature = 'historic_city_gate'][zoom >= 15],
  [feature = 'natural_cave_entrance'][zoom >= 15],
  [feature = 'man_made_mast'][zoom >= 16],
  [feature = 'man_made_tower'][zoom >= 15],
  [feature = 'man_made_water_tower'][zoom >= 15] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: darken(@man-made-icon, 20%);
    [feature = 'power_generator']['generator:source' = 'wind'],
    [feature = 'historic_city_gate'],
    [feature = 'man_made_mast'],
    [feature = 'man_made_tower'],
    [feature = 'man_made_communications_tower'],
    [feature = 'man_made_telescope'],
    [feature = 'man_made_water_tower'],
    [feature = 'man_made_storage_tank'],
    [feature = 'man_made_silo'],
    [feature = 'man_made_chimney'],
    [feature = 'man_made_crane'] {
      text-dy: 10;
    }
    [feature = 'natural_cave_entrance'] {
      text-dy: 11;
    }
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }

  [feature = 'tourism_artwork'][zoom >= 17],
  [feature = 'historic_memorial'][memorial = null][zoom >= 17],
  [feature = 'historic_memorial'][memorial != null][memorial != 'blue_plaque'][memorial != 'bust'][memorial != 'plaque'][memorial != 'stele'][memorial != 'stone'][zoom >= 17],
  [feature = 'historic_memorial'][memorial = 'statue'][zoom >= 17],
  [feature = 'historic_memorial'][memorial = 'bust'][zoom >= 18],
  [feature = 'historic_memorial'][memorial = 'stele'][zoom >= 18],
  [feature = 'historic_memorial'][memorial = 'stone'][zoom >= 18],
  [feature = 'historic_memorial'][memorial = 'blue_plaque'][zoom >= 18],
  [feature = 'historic_memorial'][memorial = 'plaque'][zoom >= 18],
  [feature = 'man_made_obelisk'][zoom >= 17],
  [feature = 'historic_monument'][zoom >= 16],
  [feature = 'historic_fort'][zoom >= 16],
  [feature = 'historic_castle'][zoom >= 16],
  [feature = 'historic_manor'][zoom >= 16] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @memorials;
    text-dy: 11;
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }

  [feature = 'amenity_fountain'][zoom >= 14] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @water-text;
    text-dy: 4;
    text-face-name: @bold-fonts;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
    [zoom >= 17] {
      text-dy: 10;
    }
  }

  [feature = 'natural_spring'][zoom >= 14] {
    text-name: "[name]";
    text-size: 10;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @water-text;
    text-face-name: @bold-fonts;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
    text-dy: 7;
  }

  [feature = 'amenity_atm'][zoom >= 16],
  [feature = 'amenity_vending_machine'][vending = 'public_transport_tickets'][zoom >= 16] {
    text-name: "[operator]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-dy: 10;
    text-fill: @amenity-brown;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
    text-face-name: @standard-font;
  }

  [feature = 'tourism_alpine_hut'][zoom >= 12][zoom <= 13],
  [feature = 'amenity_shelter'][zoom >= 13][zoom <= 13],
  [feature = 'tourism_wilderness_hut'][zoom >= 12][zoom <= 13],
  [feature = 'leisure_picnic_table'][zoom >= 16],
  [feature = 'tourism_hotel'][zoom >= 16],
  [feature = 'tourism_motel'][zoom >= 16],
  [feature = 'tourism_hostel'][zoom >= 16],
  [feature = 'tourism_chalet'][zoom >= 16],
  [feature = 'tourism_guest_house'][zoom >= 15],
  [feature = 'tourism_apartment'][zoom >= 15],
  [feature = 'tourism_camp_site'][zoom >= 15],
  [feature = 'tourism_caravan_site'][zoom >= 15], {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @accommodation-text;
    text-dy: 11;
    text-face-name: @bold-fonts;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
    [feature = 'tourism_motel'] {
      text-dy: 13;
    }
    [feature = 'tourism_camp_site'],
    [feature = 'tourism_caravan_site'] {
      text-dy: 15;
    }
    [feature = 'leisure_picnic_table'][zoom >= 16],
    [feature = 'amenity_shelter'] {
      text-fill: @man-made-icon;
    }
    [feature = 'tourism_alpine_hut'],
    [feature = 'tourism_wilderness_hut'],
    [feature = 'amenity_shelter'] {
      [access != ''][access != 'permissive'][access != 'yes'] {
        text-opacity: 0.33;
        text-halo-radius: 0;
      }
    }
  }

  [feature = 'highway_bus_stop'],
  [feature = 'amenity_charging_station'],
  [feature = 'amenity_fuel'],
  [feature = 'amenity_bus_station'] {
    [zoom >= 16] {
      text-name: "[name]";
      text-size: @standard-font-size;
      text-wrap-width: @standard-wrap-width;
      text-line-spacing: @standard-line-spacing-size;
      text-fill: @transportation-text;
      text-dy: 11;
      text-face-name: @standard-font;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      [feature = 'highway_bus_stop'] {
        text-dy: 9;
      }
      [access != ''][access != 'permissive'][access != 'yes'] {
        text-opacity: 0.33;
        text-halo-radius: 0;
      }
    }
  }

  [feature = 'shop'][shop != 'mall'] {
    [way_pixels > 3000][zoom >= 16],
    [zoom >= 16] {
      text-name: "[name]";
      text-size: @standard-font-size;
      text-wrap-width: @standard-wrap-width;
      text-line-spacing: @standard-line-spacing-size;
      text-dy: 12;
      text-fill: @shop-text;
      text-face-name: @standard-font;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: rgba(255, 255, 255, 0.6);
      [shop = 'massage'] {
        text-fill: @leisure-green;
      }
    }
  }

  [feature = 'shop_supermarket'],
  [feature = 'shop_department_store'] {
    [zoom >= 16] {
      text-name: "[name]";
      text-size: @standard-font-size;
      text-wrap-width: @standard-wrap-width;
      text-line-spacing: @standard-line-spacing-size;
      text-dy: 12;
      text-fill: @shop-text;
      text-face-name: @standard-font;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: rgba(255, 255, 255, 0.6);
    }
  }

  [feature = 'natural_scree'][zoom >= 9],
  [feature = 'natural_shingle'][zoom >= 9],
  [feature = 'natural_bare_rock'],
  [feature = 'natural_sand'] {
    [zoom >= 8][way_pixels > 3000][is_building = 'no'],
    [zoom >= 15][is_building = 'no'] {
      text-name: "[name]";
      text-size: @landcover-font-size;
      text-wrap-width: @landcover-wrap-width-size;
      text-line-spacing: @landcover-line-spacing-size;
      [way_pixels > 12000] {
        text-size: @landcover-font-size-big;
        text-wrap-width: @landcover-wrap-width-size-big;
        text-line-spacing: @landcover-line-spacing-size-big;
      }
      [way_pixels > 48000] {
        text-size: @landcover-font-size-bigger;
        text-wrap-width: @landcover-wrap-width-size-bigger;
        text-line-spacing: @landcover-line-spacing-size-bigger;
      }
      text-face-name: @landcover-face-name;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      [feature = 'natural_scree'],
      [feature = 'natural_shingle'],
      [feature = 'natural_bare_rock'] {
        text-fill: darken(@bare_ground, 50%);
      }
      [feature = 'natural_sand'] {
        text-fill: darken(@sand, 50%);
      }
    }
  }

  [feature = 'natural_glacier'][is_building = 'no'] {
    [zoom >= 8][way_pixels > 10000][zoom < 11],
    [zoom >= 10][way_pixels > 750][zoom < 15],
    [zoom >= 15][zoom < 16] {
      text-name: "[name]";
      text-size: @landcover-font-size;
      text-wrap-width: @landcover-wrap-width-size;
      text-line-spacing: @landcover-line-spacing-size;
      [way_pixels > 12000] {
        text-size: @landcover-font-size-big;
        text-wrap-width: @landcover-wrap-width-size-big;
        text-line-spacing: @landcover-line-spacing-size-big;
      }
      [way_pixels > 48000] {
        text-size: @landcover-font-size-bigger;
        text-wrap-width: @landcover-wrap-width-size-bigger;
        text-line-spacing: @landcover-line-spacing-size-bigger;
      }
      text-fill: mix(darken(@glacier, 40%), darken(@glacier-line, 30%), 50%);
      text-face-name: @bold-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
    }
  }

  [feature = 'amenity_hunting_stand'][zoom >= 16] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-dy: 10;
    text-fill: darken(@man-made-icon, 20%);
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }

  [feature = 'natural_tree'][zoom >= 16] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: green;
    text-dy: 7;
    [zoom >= 18] { text-dy: 8; }
    [zoom >= 19] { text-dy: 11; }
    [zoom >= 20] { text-dy: 18; }
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }

  [feature = 'amenity_casino'][zoom >= 16] {
    text-name: "[name]";
    text-fill: @amenity-brown;
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-dy: 10;
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }

  [feature = 'tourism_attraction'][zoom >= 16][is_building = 'no'] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: @tourism;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
    text-face-name: @standard-font;
  }

  [feature = 'amenity_parking'],
  [feature = 'amenity_bicycle_parking'],
  [feature = 'amenity_motorcycle_parking'],
  [feature = 'amenity_parking_entrance']["parking"='underground'] {
    [zoom >= 14][way_pixels > 10000],
    [zoom >= 15][way_pixels > 3000],
    [zoom >= 16] {
      text-name: "[name]";
      text-size: @standard-font-size;
      text-wrap-width: @standard-wrap-width;
      text-line-spacing: @standard-line-spacing-size;
      text-fill: @transportation-text;
      text-dy: 9;
      text-face-name: @standard-font;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      [access != ''][access != 'permissive'][access != 'yes'] {
        text-opacity: 0.33;
        text-halo-radius: 0;
      }
      [feature = 'amenity_bicycle_parking'],
      [feature = 'amenity_motorcycle_parking'] { text-dy: 12; }
    }
  }
}

#text-low-priority[zoom <= 16] {
  [feature = 'man_made_cross'][zoom >= 16],
  [feature = 'historic_wayside_cross'][zoom >= 16],
  [feature = 'historic_wayside_shrine'][zoom >= 16] {
    text-name: "[name]";
    text-size: @standard-font-size;
    text-wrap-width: @standard-wrap-width;
    text-line-spacing: @standard-line-spacing-size;
    text-fill: darken(@man-made-icon, 20%);
    text-dy: 6;
      [feature = 'historic_wayside_shrine'] { text-dy: 10; }
    text-face-name: @standard-font;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @standard-halo-fill;
  }
}

